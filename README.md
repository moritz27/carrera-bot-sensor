# Signal Analysis of a Carrera-Track


|<img src="Img/car_on_track.jpg" alt="car_on_track" width="500" />|
|:--:| 
| *Carrera Car with sensor PCB on the track* |


The aim of the project is to measure the physical parameters of a Carrera car while its driving on the track. The two values that should be measured are the rotation speed in °/sec and the alignment of the car in the three dimensional space based on the g-force.


To measure this data a Gyro and Accelerometer Sensor  [MPU6050 ](https://invensense.tdk.com/products/motion-tracking/6-axis/mpu-6050/) is placed on the car which is connected to a [ESP8266 ](https://www.espressif.com/en/products/hardware/esp8266ex/overview) module. The microprocessor is connected to a MQTT broker with WIFI. While the car is driving it is collecting the data and transmitting it to the broker where it can be visualized.


| ![connections](/Img/connections.jpg)|
|:--:| 
| *Connection Diagramm* |

The needed power supply is provided by the 12V PWM Signal for the motor that is provided thru the contacts on the track. So there is the need of a minimum PWM Signal to power up the module before the car is moving. After a short initialization process to calibrate the sensor and establish the connection the software is ready.

## 3D-Models

To mount the PCB to the car there is a custom part which can be 3d printed. It also holds a magnet for use with another project.

|<img src="Img/chassis.png" alt="chassis" width="400" />|
|:--:| 
| *3D Model* |


Another model is a body to close the whole car but its rather big and heavy and made the car in test very slow and more likely to leave the track to often so its not recommended. 

The models are available as STL files to import in a 3D Printing slicer or as a Fusion360 File to change it on your own.

## PCB

  

| <img src="Img/pcb_routing.png" alt="pcb_routing" width="300" /> | <img src="/Img/pcb_final.png" alt="pcb_final" width="300" /> |
|:--:|:--:|
| *routing of the PCB* | *3D View of the PCB* |

This repo contains different PCB files:

- KiCAD: For editing and changing the PCB (all needed parts and symbols included)
- Gerber: For direct order of the existing PCB (upload the .zip archive to the PCB distributor of your choice e.g. [JLCPCB](https://jlcpcb.com/) )

The BOM file contains a list of all needed parts to equip the PCB. The big electrolytic cap C1 can be replaced with a 3.3V Supercap to store more energy. The diode D5 (TVS) is a optional protection for the circuit. The footprint of C2 can either be THT or SMD. All other parts need to be as stated for correct function.

## Software

### Sensor 

The software is available to be compiled with two options:

- Arduino IDE: outdated
- [PlatformIO (for Visual Studio Code)](https://platformio.org/install/ide?install=vscode): newest version

For both the following library is needed which can be installed with the integrated library manager or manual:

- [pubsubclient](https://github.com/knolleary/pubsubclient?utm_source=platformio&utm_medium=piohome)

All important changes can be made in the `config.h` file in the include directory of the PlatformIO project:

```cpp
/*  ########################## USER config ########################## */
#define debug true
const bool send_acc[3] = {false, false, false};
const bool send_temp = false;
const bool send_gyro[3] = {false, false, true};

const float repeat_time = 0.02; // run cyclic_task() every x s

/* ########################## MPU6050 config ########################## */
#define mpu_slave_address 0x68
const uint16 max_g = 2; //max g-force +/-
const uint16 max_rate = 2000; //max rate degree/s

/* ########################## WIFI config ########################## */
const char* ssid = "CarreraBot_WLAN"; // Replace with your WIFI AP Name
const char* wifi_password = "CarreraBot"; // Replace with your WIFI Password

/* ########################## MQTT / OTA config ########################## */
const char* hostname = "MQTT_MPU"; //Name of the device for MQTT and OTA
const char* mqtt_server = "192.168.4.1"; //Replace with the IP of the MQTT Broker

const char* mqtt_acce_mpu_topics [3] = {"ax","ay","az"};
const char* mqtt_temp_mpu_topic = "mpu_temp";
const char* mqtt_gyro_mpu_topics [3] = {"gx","gy","gz"}; 
```

### MQTT Broker

For testing purpose there is code for using a second ESP8266 as a MQTT Broker to receive the data and put it out to the serial monitor. This code is also available for both IDEs. To compile this the [uMQTTBroker](https://github.com/martin-ger/uMQTTBroker)- Libary is needed.

### Python Script

To visualize the data stream of the serial monitor there is a python script which can run on a local machine that is connected to the broker over USB. It receives the data and displays it in a graph. To run the script some libaries need to be installed.

- matplotlib
- pyserial
- pandas

## Simulation

A LTSpice Simulation of the input filter on the PCB is in the corresponding folder. It shows the filter capability of the circuit with different PWM duty cycles.

## Documentation

There is a more detailed documentation available in German language.

## To be done..

- Upload the Python Script
- List all needed Python Libs
- Upload the newest version of the STL files
- ... 
