void setup_ota(void) {
  ArduinoOTA.setHostname(my_hostname);

  ArduinoOTA.onError([](ota_error_t error) {
    (void)error;
    ESP.restart();
  });

  ArduinoOTA.begin();
}
