void setup_mqtt(void) {
  //Serial.println("Setup MQTT");
  // Connect to MQTT Broker
  // client.connect returns a boolean value to let us know if the connection was successful.
  while (!client.connected()) {
    client.connect(my_hostname);
    delay(500);
    //Serial.print(".");
  }
  //Serial.println();
  delay(1000);
}

void send_mqtt_int(int content, const char* topic) {
  char string_to_send[10];
  sprintf(string_to_send, "%i", content);
  send_mqtt_string(string_to_send, topic);
}

void send_mqtt_float(float content, const char* topic) {
  char string_to_send[10];
  sprintf(string_to_send, " %.4f", content);
  //Serial.print("message content: "); Serial.println(string_to_send);
  send_mqtt_string(string_to_send, topic);
}

void send_mqtt_string(const char* content, const char* topic) {
  if (client.publish(topic, content)) {
    //Serial.print(topic); Serial.println(" message sent");
  }
  else {
    //Serial.print(topic); Serial.println("failed to send. Reconnecting to MQTT Broker and trying again");
    while (!client.connected()) {
      client.connect(my_hostname);
      delay(500);
      //Serial.print(".");
    }
    delay(10);
    client.publish(topic, content);
  }
}
