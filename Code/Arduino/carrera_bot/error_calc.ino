#define count 200

//----------------- Error Determination Acceleration + Gyro---------------------
// measure eyery value "count" times to calculate the mean value
// standard²=((data_n1 - mean_n1)²+(data_n2 - mean_n2)²+...)/n

void calculate_acc_errors(uint8_t device_address, int16_t *mpu_raw_acc_mean, int16_t *mpu_raw_acc_standard)
{
    int16_t mpu_raw_acc_data[3] = {0,0,0};
    int32_t mpu_raw_acc_sum[3] = {0,0,0};

    for (int j=0; j < count; j++){
        // read data
        mpu6050_read_acc_raw(device_address, &mpu_raw_acc_data[0]);
        // Sum of Acceleration raw values 
        for (int i=0; i<=2; i++){
            mpu_raw_acc_sum[i] +=  mpu_raw_acc_data[i];
            delay(1);
        } 
    }

    for (int i=0; i <= 2; i++){
            mpu_raw_acc_mean[i] = mpu_raw_acc_sum[i] / count;      
    }

    for (int j=0; j < count; j++){
        // Acceleration array value 0 to 2 for ax, ay and az
        for (int i=0; i<=2; i++){
            mpu_raw_acc_standard[i] += pow(mpu_raw_acc_data[i] - mpu_raw_acc_mean[i], 2);
        }
    }

    // Acceleration array value 0 to 2 for ax, ay and az
    for (int i=0; i <= 2; i++){
            mpu_raw_acc_standard[i] = sqrt(mpu_raw_acc_standard[i] / count);
    }
}


void calculate_gyro_errors(uint8_t device_address, int16_t *mpu_raw_gyro_mean, int16_t *mpu_raw_gyro_standard)
{
    int16_t mpu_raw_gyro_data[3] = {0,0,0};
    int32_t mpu_raw_gyro_sum[3] = {0,0,0}; 

    for (int j=0; j<count; j++){
        mpu6050_read_gyro_raw(device_address, &mpu_raw_gyro_data[0]);        
        // // Sum of Gyro raw values
        for (int i=0; i<=2; i++){
            mpu_raw_gyro_sum[i] += mpu_raw_gyro_data[i];
            delay(1);
        }
    }

    // Gyro array value 0 to 2 for gx, gy and gz
    for (int i=0; i<=2; i++){
            mpu_raw_gyro_mean[i] = mpu_raw_gyro_sum[i] / count;           
    }

    for (int j=0; j<count; j++){
        mpu6050_read_gyro_raw(device_address, &mpu_raw_gyro_data[0]);
        // Gyro array value 0 to 2 for gx, gy and gz
        for (int i=0; i<=2; i++){
            mpu_raw_gyro_standard[i] += pow(mpu_raw_gyro_data[i] - mpu_raw_gyro_mean[i], 2);
        }           
    }

    // Mean Gyro values: 0 to 2 for gx, gy and gz
    for (int i=0; i<=2; i++){
            mpu_raw_gyro_standard[i] = sqrt(mpu_raw_gyro_standard[i] / count);
    }
}   
