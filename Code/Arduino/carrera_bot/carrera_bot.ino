/* ########################## includes ########################## */
#include <ESP8266WiFi.h> // Enables the ESP8266 to connect to the local network (via WiFi)
#include <PubSubClient.h> // Allows us to connect to, and publish to the MQTT broker
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <Wire.h>

/* ########################## MPU6050 ########################## */
// MPU6050 Slave Device Address
const uint8_t mpu_slave_address = 0x68;

// Select SDA and SCL pins for I2C communication 
#define scl_pin D3
#define sda_pin D4

// sensitivity scale factor respective to full scale setting provided in datasheet 
#define accel_scale_factor  16384
#define gyro_scale_factor  16.384

/* ########################## WIFI ########################## */
const char* ssid = "CarreraBot_WLAN"; // Replace with your WIFI AP Name
const char* wifi_password = "CarreraBot"; // Replace with your WIFI Password
WiFiClient wifi_client;

/* ########################## MQTT ########################## */
const char* mqtt_server = "192.168.4.1"; //Replace with the IP of the MQTT Broker
const char* my_hostname = "MQTT_OTA_MPU"; //Name of the device
PubSubClient client(mqtt_server, 1883, wifi_client); // 1883 is the listener port for the Broker


const char* mqtt_acce_mpu_topics [3] = {"ax","ay","az"};
const char* mqtt_temp_mpu_topic = "mpu_temp";
const char* mqtt_gyro_mpu_topics [3] = {"gx","gy","gz"};

/* ########################## user variables ########################## */
const int repeat_time = 10; // do shit every x ms
unsigned long timer=0;
int error_counter = 0;        // error counter

int16_t mpu_raw_gyro_data[3] = {0,0,0}; //gyro raw data
float mpu_conv_gyro_data[3] = {0.0, 0.0, 0.0}; // gyro converted data
int16_t mpu_raw_gyro_zero[3] = {0,0,0};       // mean value
int16_t mpu_raw_gyro_standard[3] = {0,0,0};   // standard deviation

/* ########################## startup code ########################## */
void setup() {
  //Serial.begin(115200);
  Wire.begin(sda_pin, scl_pin);
  //setup_mpu6050(mpu_slave_address);
  setup_mpu(mpu_slave_address);
  setup_wfi();
  setup_mqtt();
  //setup_ota();
  calculate_gyro_errors(mpu_slave_address, &mpu_raw_gyro_zero[0], &mpu_raw_gyro_standard[0]);
}

/* ########################## loop ########################## */

void task(){
  
 
  int16_t mpu_raw_acc_data[3] = {0,0,0};
  int16_t mpu_raw_gyro_data[3] = {0,0,0};

//----------------- Error Determination Acceleration + Gyro---------------------
// measure eyery value 200 times to calculate the mean value
// standard²=((mean_n1 - data_n1)²+(mean_n2 - data_n1)²+...)/n

  double mpu_raw_acc_mean[3] = {0,0,0};       // mean value
  double mpu_raw_acc_standard[3] = {0,0,0};   // standard deviation

  double mpu_raw_gyro_mean[3] = {0,0,0};       // mean value
  double mpu_raw_gyro_standard[3] = {0,0,0};   // standard deviation

  if (error_counter == 0){ 
      // ------determine mean value------
      for (int j=0; j<200; j++){
          // read data
          mpu6050_read_acc_raw(mpu_slave_address, &mpu_raw_acc_data[0]);
          mpu6050_read_gyro_raw(mpu_slave_address, &mpu_raw_gyro_data[0]);
          //Serial.println("Schleife");Serial.println(j);Serial.println(" ");
          
          // sum data 
          // Acceleration  
          for (int i=0; i<=1; i++){
              mpu_raw_acc_mean[i] = mpu_raw_acc_mean[i] + (double)mpu_raw_acc_data[i];
              //Serial.println("Data");Serial.println((double)mpu_raw_acc_data[i]);
              //Serial.println("Mean");Serial.println((double)mpu_raw_acc_mean[i]);Serial.println(" ");
          } 
          // Gyro
          for (int i=0; i<=2; i++){
              mpu_raw_gyro_mean[i] = mpu_raw_gyro_mean[i] + (double)mpu_raw_gyro_data[i];
              //Serial.println("Data");Serial.println((double)mpu_raw_gyro_data[i]);
              //Serial.println("Mean");Serial.println((double)mpu_raw_gyro_mean[i]);Serial.println(" ");
          }
      }
      // Acceleration array value 0 to 1 for ax and ay
      for (int i=0; i<=1; i++){
             mpu_raw_acc_mean[i] = mpu_raw_acc_mean[i] / 200;     
             //Serial.println("Mittelwert double");Serial.println(mpu_raw_acc_mean[i]);       
      }
      // Gyro array value 0 to 2 for gx, gy and gz
      for (int i=0; i<=2; i++){
             mpu_raw_gyro_mean[i] = mpu_raw_gyro_mean[i] / 200;     
             //Serial.println("Mittelwert double");Serial.println(mpu_raw_gyro_mean[i]);       
      }
      
      // ------determine standard deviation------
      for (int j=0; j<200; j++){
          // read data
          mpu6050_read_acc_raw(mpu_slave_address, &mpu_raw_acc_data[0]);
          mpu6050_read_gyro_raw(mpu_slave_address, &mpu_raw_gyro_data[0]);
          //Serial.println("Schleife");Serial.println(j);Serial.println(" ");
          // sum data
          // Acceleration array value 0 to 1 for ax and ay
          for (int i=0; i<=1; i++){
              mpu_raw_acc_standard[i] = mpu_raw_acc_standard[i] + pow((double)mpu_raw_acc_data[i] - mpu_raw_acc_mean[i],2);
              //Serial.println("Data");Serial.println((double)mpu_raw_acc_data[i]);
              //Serial.println("Standard");Serial.println((double)mpu_raw_acc_standard[i]);Serial.println(" ");
          }
          // Gyro array value 0 to 2 for gx, gy and gz
          for (int i=0; i<=2; i++){
              mpu_raw_gyro_standard[i] = mpu_raw_gyro_standard[i] + pow((double)mpu_raw_gyro_data[i] - mpu_raw_gyro_mean[i],2);
              //Serial.println("Data");Serial.println((double)mpu_raw_gyro_data[i]);
              //Serial.println("Standard");Serial.println((double)mpu_raw_gyro_standard[i]);Serial.println(" ");
          }           
      }
      // Acceleration array value 0 to 1 for ax and ay
      for (int i=0; i<=1; i++){
             mpu_raw_acc_standard[i] = mpu_raw_acc_standard[i] / 200;
             mpu_raw_acc_standard[i] = sqrt(mpu_raw_acc_standard[i]); 
             //Serial.println("Standardabweilchung double");Serial.println(mpu_raw_acc_standard[i]);       
             //Serial.println("Standardabweichung int16_t");Serial.println(mpu_raw_acc_standard_int16_t[i]); 
      }
      // Gyro array value 0 to 2 for gx, gy and gz
      for (int i=0; i<=2; i++){
             mpu_raw_gyro_standard[i] = mpu_raw_gyro_standard[i] / 200;
             mpu_raw_gyro_standard[i] = sqrt(mpu_raw_gyro_standard[i]); 
             //Serial.println("Standardabweilchung double");Serial.println(mpu_raw_gyro_standard[i]);       
             //Serial.println("Standardabweichung int16_t");Serial.println(mpu_raw_gyro_standard_int16_t[i]); 
      }      
      error_counter = 1;
  }    
  else {
//--------------------------------------------
  


  mpu6050_read_acc_raw(mpu_slave_address, &mpu_raw_acc_data[0]);
  for (int i=0; i<=2; i++){
    //Serial.print((double)mpu_raw_acc_data[i] / accel_scale_factor);Serial.print(" ");
    send_mqtt_float((double)mpu_raw_acc_data[i] - mpu_raw_acc_standard[i], mqtt_acce_mpu_topics[i]) ;
  }

  //double mpu_temp_data;
  //mpu_temp_data = mpu6050_read_temp(mpu_slave_address);
  //send_mqtt_float(mpu_temp_data, mqtt_temp_mpu_topic);
  
  //int16_t mpu_raw_gyro_data[3];
  //mpu6050_read_gyro_raw(mpu_slave_address, &mpu_raw_gyro_data[0]);
  //for (int i=0; i<=2; i++){
  //   Serial.print(mpu_raw_gyro_data[i]);Serial.print(" ");
  //   send_mqtt_float(mpu_raw_gyro_data[i] - mpu_raw_gyro_standard[i], mqtt_gyro_mpu_topics[i]) ;
  //  } 
  //Serial.println();
  }
}

void task2 (void){
      mpu6050_read_gyro_raw(mpu_slave_address, &mpu_raw_gyro_data[0]);
/*     for (int i=0; i<=2; i++){
        if (((double)mpu_raw_gyro_zero[i] / gyro_scale_factor) > 0.0){
            mpu_conv_gyro_data [i] = (double)(mpu_raw_gyro_data[i] - mpu_raw_gyro_zero[i]) / gyro_scale_factor;
            Serial.print(mpu_conv_gyro_data[i]);Serial.print(" | ");
        }
        else if (((double)mpu_raw_gyro_zero[i] / gyro_scale_factor) < 0.0) {
            mpu_conv_gyro_data [i] = (double)(mpu_raw_gyro_data[i] + mpu_raw_gyro_zero[i]) / gyro_scale_factor;
            Serial.print(mpu_conv_gyro_data[i]);Serial.print(" | ");
        }
        else{
            mpu_conv_gyro_data [i] = (double)(mpu_raw_gyro_data[i])  / gyro_scale_factor;
            Serial.print(mpu_conv_gyro_data[i]);Serial.print(" | ");
        }
        //send_mqtt_float(my_mqtt_client, mpu_conv_gyro_data [i], mqtt_gyro_mpu_topics[i], my_hostname);
    } */


    if (((double)mpu_raw_gyro_zero[2] / gyro_scale_factor) > 0.0){
        mpu_conv_gyro_data [2] = (double)(mpu_raw_gyro_data[2] - mpu_raw_gyro_zero[2]) / gyro_scale_factor;
        Serial.print(mpu_conv_gyro_data[2]);
    }
    else if (((double)mpu_raw_gyro_zero[2] / gyro_scale_factor) < 0.0) {
        mpu_conv_gyro_data [2] = (double)(mpu_raw_gyro_data[2] + mpu_raw_gyro_zero[2]) / gyro_scale_factor;
        Serial.print(mpu_conv_gyro_data[2]);
    }
    else{
        mpu_conv_gyro_data [2] = (double)(mpu_raw_gyro_data[2])  / gyro_scale_factor;
        Serial.print(mpu_conv_gyro_data[2]);
    }
    send_mqtt_float(mpu_conv_gyro_data [2], mqtt_gyro_mpu_topics[2]);

    //delay(1);
    Serial.println(); 
}

void loop() {
  if (millis() - timer >= repeat_time){
    task2();
    timer = millis();
  }
  //ArduinoOTA.handle();
}
