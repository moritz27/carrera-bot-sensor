//// MPU6050 few configuration register addresses
//const uint8_t MPU6050_REGISTER_SMPLRT_DIV   =  0x19;
//const uint8_t MPU6050_REGISTER_USER_CTRL    =  0x6A;
//const uint8_t MPU6050_REGISTER_PWR_MGMT_1   =  0x6B;
//const uint8_t MPU6050_REGISTER_PWR_MGMT_2   =  0x6C;
//const uint8_t MPU6050_REGISTER_CONFIG       =  0x1A;
//const uint8_t MPU6050_REGISTER_GYRO_CONFIG  =  0x1B;
//const uint8_t MPU6050_REGISTER_ACCEL_CONFIG =  0x1C;
//const uint8_t MPU6050_REGISTER_FIFO_EN      =  0x23;
//const uint8_t MPU6050_REGISTER_INT_ENABLE   =  0x38;
//const uint8_t MPU6050_REGISTER_ACCEL_XOUT_H =  0x3B;
//const uint8_t MPU6050_REGISTER_SIGNAL_PATH_RESET  = 0x68;
//
//
//void setup_mpu6050(uint8_t device_address) {
//  delay(150);
//
//  // gyroscope output rate divider Sample Rate = Gyroscope Output Rate / (1 + SMPLRT_DIV)
//  // Sample Rate = 8kHz / (1 + 7) = 1kHz
//  I2C_Write(device_address, MPU6050_REGISTER_SMPLRT_DIV, 0x07);
//
//  // Internal 8MHz clock at startup then PLL with X axis gyroscope reference
//  I2C_Write(device_address, MPU6050_REGISTER_PWR_MGMT_1, 0x01);
//
//  // Low Power disabled
//  I2C_Write(device_address, MPU6050_REGISTER_PWR_MGMT_2, 0x00);
//
//  // Frame Synchronization (FSYNC) pin sampling = OFF and the DigitalLow Pass Filter (DLPF) = OFF
//  I2C_Write(device_address, MPU6050_REGISTER_CONFIG, 0x00); //
//
//  //set +/-250 degree/second full scale, no selftest
//  I2C_Write(device_address, MPU6050_REGISTER_GYRO_CONFIG, 0x00);
//
//  // set +/- 2g full scale, no selftest
//  I2C_Write(device_address, MPU6050_REGISTER_ACCEL_CONFIG, 0x00);
//
//  // FIFO deactivated
//  I2C_Write(device_address, MPU6050_REGISTER_FIFO_EN, 0x00);
//
//  // Data Ready interrupt enable
//  I2C_Write(device_address, MPU6050_REGISTER_INT_ENABLE, 0x01);
//
//  // no reset of the analog and digital signal paths
//  I2C_Write(device_address, MPU6050_REGISTER_SIGNAL_PATH_RESET, 0x00);
//
//  // no  FIFO buffer, no I2C Master Mode, and no primary I2C
//  I2C_Write(device_address, MPU6050_REGISTER_USER_CTRL, 0x00);
//
//}
//
//void I2C_Write(uint8_t device_address, uint8_t register_address, uint8_t data) {
//  Wire.beginTransmission(device_address);
//  Wire.write(register_address);
//  Wire.write(data);
//  Wire.endTransmission();
//}
//
//void mpu6050_read_acc_raw(uint8_t device_address, int16_t *raw_acc_data) {
//  Wire.beginTransmission(device_address);
//  Wire.write(0x3B);
//  Wire.endTransmission();
//  Wire.requestFrom(device_address,(uint8_t)6);
//  for (int i = 0; i <= Wire.available(); i++) {
//    raw_acc_data[i] = (((int16_t)Wire.read() << 8) | Wire.read());
//  }
//}
//
//double mpu6050_read_temp(uint8_t device_address) {
//  int16_t raw_temp_data;
//  double temp_data;
//  Wire.beginTransmission(device_address);
//  Wire.write(0x41);
//  Wire.endTransmission();
//  Wire.requestFrom(device_address,(uint8_t)2);
//  raw_temp_data = (((int16_t)Wire.read() << 8) | Wire.read());
//  temp_data = raw_temp_data / 340 + 36.53;
//  return temp_data;
//}
//
//void mpu6050_read_gyro_raw(uint8_t device_address, int16_t *raw_gyro_data) {
//  Wire.beginTransmission(device_address);
//  Wire.write(0x43);
//  Wire.endTransmission();
//  Wire.requestFrom(device_address,(uint8_t)6);
//  for (int i = 0; i <= Wire.available(); i++) {
//    raw_gyro_data[i] = (((int16_t)Wire.read() << 8) | Wire.read());
//  }
//}
