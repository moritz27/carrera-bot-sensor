#include <ESP8266WiFi.h>
#include "uMQTTBroker.h"

/*
 * Your WiFi config here
 */

// Broker IP: 192.168.4.1
char ssid[] = "delay_test";     // your network SSID (name)
char pass[] = "123456789";          // your network password
bool WiFiAP = true;

#define output_pin 12


/*
 * Custom broker class with overwritten callback functions
 */
class myMQTTBroker: public uMQTTBroker
{
public:
    virtual bool onConnect(IPAddress addr, uint16_t client_count) {
      Serial.println(addr.toString()+" connected");
      return true;
    }
    
    virtual bool onAuth(String username, String password) {
      Serial.println("Username/Password: "+username+"/"+password);
      return true;
    }
    
    virtual void onData(String topic, const char *data, uint32_t length) {
      char data_str[length+1];
      os_memcpy(data_str, data, length);
      data_str[length] = '\0';

      
      //Serial.println("received topic '"+topic+"' with data '"+(String)data_str+"'");
      Serial.println(topic+" "+(String)data_str);
      if (topic == "delay_test" && (String)data_str == "ON"){
        digitalWrite(output_pin,HIGH);
      }
      else if (topic == "delay_test" && (String)data_str == "OFF"){
        digitalWrite(output_pin,LOW);
      }
      else{;}
    }
};

myMQTTBroker myBroker;

/*
 * WiFi init stuff
 */
void startWiFiClient()
{
  Serial.println("Connecting to "+(String)ssid);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, pass);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  
  Serial.println("WiFi connected");
  Serial.println("IP address: " + WiFi.localIP().toString());
}

void startWiFiAP()
{
  WiFi.mode(WIFI_AP);
  WiFi.softAP(ssid, pass);
  Serial.println("AP started");
  Serial.println("IP address: " + WiFi.softAPIP().toString());
}

//int counter = 0;

void setup()
{
  Serial.begin(115200);
  Serial.println();
  Serial.println();

  pinMode(output_pin,OUTPUT);

  // Start WiFi
  if (WiFiAP)
    startWiFiAP();
  else
    startWiFiClient();

  // Start the broker
  Serial.println("Hallo I bims 1 Broker");
  myBroker.init();

/*
 * Subscribe to anything
 */
 myBroker.subscribe("#");
 
}

void loop()
{

}
