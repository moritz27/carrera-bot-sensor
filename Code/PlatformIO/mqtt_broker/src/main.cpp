#include <Arduino.h>

/*
 * uMQTTBroker demo for Arduino (C++-style)
 * 
 * The program defines a custom broker class with callbacks, 
 * starts it, subscribes locally to anything, and publishs a topic every second.
 * Try to connect from a remote client and publish something - the console will show this as well.
 */

#include <ESP8266WiFi.h>
#include <uMQTTBroker.h>

/*
 * Your WiFi config here
 */

// Broker IP: 192.168.4.1
char ssid[] = "CarreraBot_WLAN";     // your network SSID (name)
char pass[] = "CarreraBot";          // your network password
bool WiFiAP = true;

String ax, ay, az, temp, gx, gy, gz;


bool new_data = false;
/*
 * Custom broker class with overwritten callback functions
 */
class myMQTTBroker: public uMQTTBroker
{
public:
    virtual bool onConnect(IPAddress addr, uint16_t client_count) {
      Serial.println(addr.toString()+" connected");
      return true;
    }
    
    virtual bool onAuth(String username, String password) {
      Serial.println("Username/Password: "+username+"/"+password);
      return true;
    }
    
    virtual void onData(String topic, const char *data, uint32_t length) {
      char data_str[length+1];
      os_memcpy(data_str, data, length);
      data_str[length] = '\0';

      new_data = true;
      //Serial.println("received topic '"+topic+"' with data '"+(String)data_str+"'");
      //Serial.println(topic+" "+(String)data_str); 
      if (topic == "gx"){
        gx = ((String)data_str);
      }
      else if (topic == "gy"){
        gy = ((String)data_str);
      }
      else if (topic == "gz"){
        gz = ((String)data_str);
      }
      else if (topic == "temp"){
        temp = ((String)data_str);
      }
      else if (topic == "ax"){
        ax = ((String)data_str);
      }
      else if (topic == "ay"){
        ay = ((String)data_str);
      }
      else if (topic == "az"){
        az = ((String)data_str);
      }            
    }
};

myMQTTBroker myBroker;

/*
 * WiFi init stuff
 */
void startWiFiClient()
{
  Serial.println("Connecting to "+(String)ssid);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, pass);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  
  Serial.println("WiFi connected");
  Serial.println("IP address: " + WiFi.localIP().toString());
}

void startWiFiAP()
{
  WiFi.mode(WIFI_AP);
  WiFi.softAP(ssid, pass);
  Serial.println("AP started");
  Serial.println("IP address: " + WiFi.softAPIP().toString());
}

//int counter = 0;

void setup()
{
  Serial.begin(115200);
  Serial.println();
  Serial.println();

  // Start WiFi
  if (WiFiAP)
    startWiFiAP();
  else
    startWiFiClient();

  // Start the broker
  Serial.println("Hallo I bims 1 Broker");
  myBroker.init();

/*
 * Subscribe to anything
 */
 myBroker.subscribe("#");
 
}

void loop()
{
  if (new_data == true){
    Serial.println(ax + " " + ay + " " + az + " " + temp + " " + gx + " " + gy+ " " +  gz);
    new_data = false;
  }
  

}