#include "ota.h"

void setup_ota(const char* hostname) {
  ArduinoOTA.setHostname(hostname);

  ArduinoOTA.onError([](ota_error_t error) {
    (void)error;
    ESP.restart();
  });

  ArduinoOTA.begin();
}
