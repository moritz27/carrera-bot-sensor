# include "wifi.h"

//WiFiClient& setup_wifi(const char* ssid, const char* wifi_password){
void setup_wifi(WiFiClient& wifi_client, const char* ssid, const char* wifi_password){

    Serial.println("Setup WIFI");

    //WiFiClient wifi_client;  

    WiFi.mode(WIFI_STA);

    WiFi.begin(ssid, wifi_password);

    while (WiFi.waitForConnectResult() != WL_CONNECTED) {
        WiFi.begin(ssid, wifi_password);
        Serial.println("Retrying connection...");
    }
    // Wait until the connection has been confirmed before continuing
    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
    }
    Serial.println();
    // Debugging - Output the IP Address of the ESP8266
    Serial.println("WiFi connected");
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());

    //return wifi_client;
}
