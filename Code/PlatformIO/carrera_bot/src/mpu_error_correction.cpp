#include "mpu_error_correction.h"

#define count 10

//----------------- Error Determination Acceleration + Gyro---------------------
// measure eyery value "count" times to calculate the mean value
// standard²=((data_n1 - mean_n1)²+(data_n2 - mean_n2)²+...)/n

void calculate_acc_errors(uint8_t device_address, int16_t *mpu_raw_acc_mean, int16_t *mpu_raw_acc_standard){
    int16_t mpu_raw_acc_data[3] = {0,0,0};
    int32_t mpu_raw_acc_sum[3] = {0,0,0};

    for (int j=0; j < count; j++){
        // read data
        mpu6050_read_acc_raw(device_address, &mpu_raw_acc_data[0]);
        // Sum of Acceleration raw values 
        for (int i=0; i<=2; i++){
            mpu_raw_acc_sum[i] +=  mpu_raw_acc_data[i];
            delay(1);
        } 
    }

    for (int i=0; i <= 2; i++){
            mpu_raw_acc_mean[i] = mpu_raw_acc_sum[i] / count;      
    }

    for (int j=0; j < count; j++){
        // Acceleration array value 0 to 2 for ax, ay and az
        for (int i=0; i<=2; i++){
            mpu_raw_acc_standard[i] += pow(mpu_raw_acc_data[i] - mpu_raw_acc_mean[i], 2);
        }
    }

    // Acceleration array value 0 to 2 for ax, ay and az
    for (int i=0; i <= 2; i++){
            mpu_raw_acc_standard[i] = sqrt(mpu_raw_acc_standard[i] / count);
    }
}

void calc_real_acc_data(const uint16_t max_g, int16_t *mpu_raw_acc_data, int16_t *mpu_raw_acc_zero, float *mpu_conv_acc_data){
        uint16_t accel_scale_factor;
        // 2^(16-1)/(g-force) 
        if (max_g == 2){
            accel_scale_factor = 16384; //+/- 2g
        }
        else if (max_g == 4){
            accel_scale_factor = 8192; // +/- 4g
        }
        else if (max_g == 8){
            accel_scale_factor = 4096; // +/- 8g
        }
        else if (max_g == 16){
            accel_scale_factor = 2048; // +/- 16g
        }

        for (int i=0; i<=2; i++){
            if (i == 2){
                if (((float)mpu_raw_acc_zero[2] / accel_scale_factor) > 1.0){
                    mpu_conv_acc_data [2] = (float)(mpu_raw_acc_data[2]) / accel_scale_factor - (1.0 - float(mpu_raw_acc_zero[2]) / accel_scale_factor);
                }
                else if (((float)mpu_raw_acc_zero[2] / accel_scale_factor) < 1.0) {
                    mpu_conv_acc_data [2] = (float)(mpu_raw_acc_data[2]) / accel_scale_factor + (1.0 - float(mpu_raw_acc_zero[2]) / accel_scale_factor);
                }
                else{
                    mpu_conv_acc_data [2] = (float)(mpu_raw_acc_data[2]) / accel_scale_factor;
                }
            }
            else{
                if (((float)mpu_raw_acc_zero[i] / accel_scale_factor)>0.0){
                    mpu_conv_acc_data [i] = (float)(mpu_raw_acc_data[i] - mpu_raw_acc_zero[i]) / accel_scale_factor;
                }
                else if (((float)mpu_raw_acc_zero[i] / accel_scale_factor)<0.0) {
                    mpu_conv_acc_data [i] = (float)(mpu_raw_acc_data[i] + mpu_raw_acc_zero[i]) / accel_scale_factor;
                }
                else{
                    mpu_conv_acc_data [i] = (float)(mpu_raw_acc_data[i])  / accel_scale_factor;
                }
            }
        }
}


void calculate_gyro_errors(uint8_t device_address, int16_t *mpu_raw_gyro_mean, int16_t *mpu_raw_gyro_standard){
    int16_t mpu_raw_gyro_data[3] = {0,0,0};
    int32_t mpu_raw_gyro_sum[3] = {0,0,0}; 

    for (int j=0; j<count; j++){
        mpu6050_read_gyro_raw(device_address, &mpu_raw_gyro_data[0]);        
        // // Sum of Gyro raw values
        for (int i=0; i<=2; i++){
            mpu_raw_gyro_sum[i] += mpu_raw_gyro_data[i];
            delay(1);
        }
    }

    // Gyro array value 0 to 2 for gx, gy and gz
    for (int i=0; i<=2; i++){
            mpu_raw_gyro_mean[i] = mpu_raw_gyro_sum[i] / count;           
    }

    for (int j=0; j<count; j++){
        mpu6050_read_gyro_raw(device_address, &mpu_raw_gyro_data[0]);
        // Gyro array value 0 to 2 for gx, gy and gz
        for (int i=0; i<=2; i++){
            mpu_raw_gyro_standard[i] += pow(mpu_raw_gyro_data[i] - mpu_raw_gyro_mean[i], 2);
        }           
    }

    // Mean Gyro values: 0 to 2 for gx, gy and gz
    for (int i=0; i<=2; i++){
            mpu_raw_gyro_standard[i] = sqrt(mpu_raw_gyro_standard[i] / count);
    }
}   

void calc_real_gyro_data(const uint16_t max_rate, int16_t *mpu_raw_gyro_data, int16_t *mpu_raw_gyro_zero, float *mpu_conv_gyro_data){
    float gyro_scale_factor;
    // 2^(16-1)/(g-force) 
    if (max_rate == 250){
        gyro_scale_factor = 131.072; //+/- 250 °/s
    }
    else if (max_rate == 500){
        gyro_scale_factor = 65.536; //+/- 500 °/s
    }
    else if (max_rate == 1000){
        gyro_scale_factor = 37.768; //+/- 1000 °/s
    }
    else if (max_rate == 2000){
        gyro_scale_factor = 16.384; //+/- 2000 °/s
    }

    for (int i=0; i<=2; i++){
        if (((float)mpu_raw_gyro_zero[i] / gyro_scale_factor) > 0.0){
            mpu_conv_gyro_data [i] = (float)(mpu_raw_gyro_data[i] - mpu_raw_gyro_zero[i]) / gyro_scale_factor;
        }
        else if (((float)mpu_raw_gyro_zero[i] / gyro_scale_factor) < 0.0) {
            mpu_conv_gyro_data [i] = (float)(mpu_raw_gyro_data[i] + mpu_raw_gyro_zero[i]) / gyro_scale_factor;
        }
        else{
            mpu_conv_gyro_data [i] = (float)(mpu_raw_gyro_data[i])  / gyro_scale_factor;
        }
    }
}