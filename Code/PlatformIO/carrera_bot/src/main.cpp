/* ########################## libary includes ########################## */
#include <Arduino.h>
#include <PubSubClient.h>
#include <Ticker.h> 

/* ########################## user includes ########################## */
#include "config.h"
#include "wifi.h"
#include "ota.h"
#include "mpu.h"
#include "mpu_error_correction.h"

/* ########################## global defines ########################## */
#define LED_PIN 12

/* ########################## global variables ########################## */
int16_t led_brigthness = 0;
int16_t mpu_raw_acc_zero[3] = {0,0,0};       // mean value
int16_t mpu_raw_acc_standard[3] = {0,0,0};   // standard deviation
int16_t mpu_raw_gyro_zero[3] = {0,0,0};       // mean value
int16_t mpu_raw_gyro_standard[3] = {0,0,0};   // standard deviation

/* ########################## initialization ########################## */
WiFiClient wifi_client;
PubSubClient client(mqtt_server, 1883, wifi_client);
Ticker task_manager;

/* ########################## functions ########################## */
void send_mqtt_string(const char* content, const char* topic) {
  if (client.publish(topic, content)) {
    //Serial.print(topic); Serial.println(" message sent");
  }
  else {
    //Serial.print(topic); Serial.println("failed to send. Reconnecting to MQTT Broker and trying again");
    while (!client.connected()) {
      client.connect(hostname);
      delay(500);
      Serial.print(".");
    }
    delay(10);
    client.publish(topic, content);
  }
}

void send_mqtt_int(int content, const char* topic) {
  char string_to_send[10];
  sprintf(string_to_send, "%i", content);
  send_mqtt_string(string_to_send, topic);
}

void send_mqtt_float(float content, const char* topic) {
  char string_to_send[10];
  sprintf(string_to_send, " %.4f", content);
  //Serial.print("message content: "); Serial.println(string_to_send);
  send_mqtt_string(string_to_send, topic);
}

void cyclic_task(){
    if (led_brigthness >=1024){
      led_brigthness = 0;
    }
    analogWrite(LED_PIN,led_brigthness);
    led_brigthness += 30;
    // run code for accelerometer only if one of the values should be send
    if ((send_acc[0] == true) | (send_acc[1]== true) | (send_acc[2]== true)){
      int16_t mpu_raw_acc_data[3] = {0,0,0}; //acc raw data
      float mpu_conv_acc_data[3] = {0.0, 0.0, 0.0}; // acc converted data
      mpu6050_read_acc_raw(mpu_slave_address, &mpu_raw_acc_data[0]);
      //calc_real_acc_data(accel_scale_factor, &mpu_raw_acc_data[0], &mpu_raw_acc_zero[0], &mpu_conv_acc_data[0]);
      calc_real_acc_data(max_g, &mpu_raw_acc_data[0], &mpu_raw_acc_zero[0], &mpu_conv_acc_data[0]);
      for (int i=0; i<=2; i++){
          if (send_acc[i] == true){
            if (debug == true){
              Serial.print(mpu_conv_acc_data[i]); Serial.print(" | ");
            }
            send_mqtt_float(mpu_conv_acc_data[i], mqtt_acce_mpu_topics[i]);
          }
      }
      if (debug == true){
        Serial.println();
      }
    }

    // run code for temperature only if one of the values should be send
    if (send_temp == true){
      float mpu_temp_data;
      mpu_temp_data = mpu6050_read_temp(mpu_slave_address);
      send_mqtt_float(mpu_temp_data, mqtt_temp_mpu_topic);
    }
    
    // run code for gyro only if one of the values should be send
    if ((send_gyro[0] == true) | (send_gyro[1] == true) | (send_gyro[2]== true)){
      int16_t mpu_raw_gyro_data[3] = {0,0,0}; //acc raw data
      float mpu_conv_gyro_data[3] = {0.0, 0.0, 0.0}; // acc converted data
      mpu6050_read_gyro_raw(mpu_slave_address, &mpu_raw_gyro_data[0]);
      //calc_real_gyro_data(gyro_scale_factor, &mpu_raw_gyro_data[0], &mpu_raw_gyro_zero[0], &mpu_conv_gyro_data[0]);
      calc_real_gyro_data(max_rate, &mpu_raw_gyro_data[0], &mpu_raw_gyro_zero[0], &mpu_conv_gyro_data[0]); 
      for (int i=0; i<=2; i++){
          if (send_gyro[i] == true){
            if (debug == true){
              Serial.print(mpu_conv_gyro_data[i]); Serial.print(" | ");
            }
            Serial.println("now send: "+String(i));
            send_mqtt_float(mpu_conv_gyro_data[i], mqtt_acce_mpu_topics[i]);
          }
      }
      if (debug == true){
        Serial.println();
      }
    }
}
/* ########################## setup ########################## */
void setup() {
    pinMode(LED_PIN, OUTPUT);

    if (debug == true){
      Serial.begin(115200);
      Serial.println("Debug mode - Serial output");
    }
    //setup_ota(hostname);
    setup_wifi(wifi_client, ssid, wifi_password);

    if (debug == true){
    Serial.println(client.state());
    }
    while (!client.connected()) {
        client.connect(hostname);
        delay(500);
        Serial.print(".");
    }
    if (debug == true){
    Serial.println(client.state());
    }
    setup_mpu(mpu_slave_address, max_g, max_rate);
    delay(2000);
    if ((send_acc[0] == true) | (send_acc[1]== true) | (send_acc[2]== true)){
      calculate_acc_errors(mpu_slave_address, &mpu_raw_acc_zero[0], &mpu_raw_acc_standard[0]);
      if (debug == true){ 
        Serial.print("mpu_raw_acc_zero X: " + String(mpu_raw_acc_zero[0]) + "| mpu_raw_acc_standard X: " + String(mpu_raw_acc_standard[0]));
        Serial.print("mpu_raw_acc_zero Y: " + String(mpu_raw_acc_zero[1]) + "| mpu_raw_acc_standard Y: " + String(mpu_raw_acc_standard[1]));
        Serial.print("mpu_raw_acc_zero Z: " + String(mpu_raw_acc_zero[2]) + "| mpu_raw_acc_standard Z: " + String(mpu_raw_acc_standard[2]));
        Serial.println();
      }
    }

    if ((send_gyro[0] == true) | (send_gyro[1] == true) | (send_gyro[2]== true)){
      calculate_gyro_errors(mpu_slave_address, &mpu_raw_gyro_zero[0], &mpu_raw_gyro_standard[0]);
      if (debug == true){
        Serial.print("mpu_raw_gyro_zero X: " + String(mpu_raw_gyro_zero[0]) + "| mpu_raw_gyro_standard X: " + String(mpu_raw_gyro_standard[0]));
        Serial.print("mpu_raw_gyro_zero Y: " + String(mpu_raw_gyro_zero[1]) + "| mpu_raw_gyro_standard Y: " + String(mpu_raw_gyro_standard[1]));
        Serial.print("mpu_raw_gyro_zero Z: " + String(mpu_raw_gyro_zero[2]) + "| mpu_raw_gyro_standard Z: " + String(mpu_raw_gyro_standard[2]));
        Serial.println();
      }
    }

    delay(1000);
    task_manager.attach(repeat_time, cyclic_task);
}

/* ########################## loop ########################## */
void loop() {
    
}