/*  ########################## USER config ########################## */
#define debug true
const bool send_acc[3] = {false, false, false};
const bool send_temp = false;
const bool send_gyro[3] = {false, false, true};

const float repeat_time = 0.02; // run cyclic_task() every x s

/* ########################## MPU6050 config ########################## */
#define mpu_slave_address 0x68
const uint16 max_g = 2; //max g-force +/-
const uint16 max_rate = 2000; //max rate degree/s

/* ########################## WIFI config ########################## */
const char* ssid = "CarreraBot_WLAN"; // Replace with your WIFI AP Name
const char* wifi_password = "CarreraBot"; // Replace with your WIFI Password

/* ########################## MQTT / OTA config ########################## */
const char* hostname = "MQTT_MPU"; //Name of the device for MQTT and OTA
const char* mqtt_server = "192.168.4.1"; //Replace with the IP of the MQTT Broker

const char* mqtt_acce_mpu_topics [3] = {"ax","ay","az"};
const char* mqtt_temp_mpu_topic = "mpu_temp";
const char* mqtt_gyro_mpu_topics [3] = {"gx","gy","gz"}; 


