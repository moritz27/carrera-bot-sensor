#include "mpu.h"

void calculate_acc_errors(uint8_t device_address, int16_t *mpu_raw_acc_mean, int16_t *mpu_raw_acc_standard);
void calculate_gyro_errors(uint8_t device_address, int16_t *mpu_raw_gyro_mean, int16_t *mpu_raw_gyro_standard);
void calc_real_acc_data(const uint16_t max_g, int16_t *mpu_raw_acc_data, int16_t *mpu_raw_acc_zero, float *mpu_conv_acc_data);
void calc_real_gyro_data(const uint16_t max_rate, int16_t *mpu_raw_gyro_data, int16_t *mpu_raw_gyro_zero, float *mpu_conv_gyro_data);