#include <Arduino.h>
#include <Wire.h>

void setup_mpu(uint8_t device_address, const uint16_t max_rate, const uint16_t max_g);
void I2C_Write(uint8_t device_address, uint8_t register_address, uint8_t data);
void mpu6050_read_acc_raw(uint8_t device_address, int16_t *raw_acc_data);
double mpu6050_read_temp(uint8_t device_address);
void mpu6050_read_gyro_raw(uint8_t device_address, int16_t *raw_gyro_data);
