EESchema Schematic File Version 4
LIBS:esp_mpu_step_down_minimum-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 3
Title "Carrera Sensor Board "
Date "2019-10-29"
Rev "V1"
Comp "Hochschule Coburg"
Comment1 "Ersteller: Moritz Vierneusel"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Sensor_Motion:MPU-6050 U3
U 1 1 5DA62DA2
P 5000 3300
F 0 "U3" H 4650 2700 50  0000 C CNN
F 1 "MPU-6050" H 5300 2700 50  0000 C CNN
F 2 "Sensor_Motion:InvenSense_QFN-24_4x4mm_P0.5mm" H 5000 2500 50  0001 C CNN
F 3 "https://store.invensense.com/datasheets/invensense/MPU-6050_DataSheet_V3%204.pdf" H 5000 3150 50  0001 C CNN
	1    5000 3300
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR012
U 1 1 5DA63BFD
P 3900 2200
F 0 "#PWR012" H 3900 2050 50  0001 C CNN
F 1 "+3.3V" H 3915 2373 50  0000 C CNN
F 2 "" H 3900 2200 50  0001 C CNN
F 3 "" H 3900 2200 50  0001 C CNN
	1    3900 2200
	1    0    0    -1  
$EndComp
$Comp
L Device:R R8
U 1 1 5DA6434A
P 3900 2500
F 0 "R8" H 3730 2560 50  0000 L CNN
F 1 "4k7" H 3700 2450 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 3830 2500 50  0001 C CNN
F 3 "~" H 3900 2500 50  0001 C CNN
	1    3900 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	4300 3100 4100 3100
Wire Wire Line
	3700 3000 3900 3000
Text HLabel 3700 3100 0    50   Input ~ 0
SCL
Text HLabel 3700 3000 0    50   Input ~ 0
SDA
Wire Wire Line
	3900 2250 3900 2200
Wire Wire Line
	4100 2250 3900 2250
Wire Wire Line
	4100 2250 4900 2250
Wire Wire Line
	4900 2250 4900 2600
Connection ~ 4100 2250
Wire Wire Line
	4900 2250 5100 2250
Wire Wire Line
	5100 2250 5100 2600
Connection ~ 4900 2250
Connection ~ 3900 3000
Wire Wire Line
	3900 3000 4300 3000
Connection ~ 4100 3100
Wire Wire Line
	4100 3100 3700 3100
Wire Wire Line
	3900 2650 3900 3000
Wire Wire Line
	4100 2500 4100 2250
Wire Wire Line
	3900 2350 3900 2250
Connection ~ 3900 2250
$Comp
L power:GND #PWR013
U 1 1 5DA67F8C
P 5000 4300
F 0 "#PWR013" H 5000 4050 50  0001 C CNN
F 1 "GND" H 5005 4127 50  0000 C CNN
F 2 "" H 5000 4300 50  0001 C CNN
F 3 "" H 5000 4300 50  0001 C CNN
	1    5000 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 4300 5000 4250
Wire Wire Line
	5000 4250 4150 4250
Wire Wire Line
	4150 4250 4150 3600
Wire Wire Line
	4150 3600 4300 3600
Connection ~ 5000 4250
Wire Wire Line
	5000 4250 5000 4000
Wire Wire Line
	4150 3600 4150 3500
Wire Wire Line
	4150 3500 4300 3500
Connection ~ 4150 3600
Wire Wire Line
	6350 3200 6350 3300
Connection ~ 6350 3300
Text HLabel 5800 3000 2    50   Input ~ 0
INT
Wire Wire Line
	5800 3000 5700 3000
Wire Wire Line
	5700 3200 6350 3200
Wire Wire Line
	5700 3300 6350 3300
Wire Wire Line
	5000 4250 6350 4250
$Comp
L Device:C C7
U 1 1 5DA6B6DC
P 6000 3500
F 0 "C7" V 6100 3650 50  0000 C CNN
F 1 "2200pF" V 6100 3350 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6038 3350 50  0001 C CNN
F 3 "~" H 6000 3500 50  0001 C CNN
	1    6000 3500
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C8
U 1 1 5DA6BF0C
P 6000 3800
F 0 "C8" V 6100 3950 50  0000 C CNN
F 1 "100nF" V 6100 3650 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6038 3650 50  0001 C CNN
F 3 "~" H 6000 3800 50  0001 C CNN
	1    6000 3800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5700 3500 5850 3500
Wire Wire Line
	5700 3600 5700 3800
Wire Wire Line
	5700 3800 5850 3800
Wire Wire Line
	6150 3500 6350 3500
Connection ~ 6350 3500
Wire Wire Line
	6350 3500 6350 3300
Wire Wire Line
	6150 3800 6350 3800
Wire Wire Line
	6350 3500 6350 3800
Connection ~ 6350 3800
Wire Wire Line
	6350 3800 6350 4250
$Comp
L Device:C C9
U 1 1 5DA6D7F7
P 6350 2550
F 0 "C9" H 6500 2650 50  0000 C CNN
F 1 "100nF" H 6500 2450 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6388 2400 50  0001 C CNN
F 3 "~" H 6350 2550 50  0001 C CNN
	1    6350 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 2400 6350 2250
Wire Wire Line
	6350 2250 5100 2250
Connection ~ 5100 2250
Wire Wire Line
	6350 2700 6350 3200
Connection ~ 6350 3200
Wire Wire Line
	4100 2800 4100 3100
$Comp
L Device:R R9
U 1 1 5DA64794
P 4100 2650
F 0 "R9" H 4170 2696 50  0000 L CNN
F 1 "4k7" H 4170 2605 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 4030 2650 50  0001 C CNN
F 3 "~" H 4100 2650 50  0001 C CNN
	1    4100 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	4300 3200 4150 3200
Wire Wire Line
	4150 3200 4150 3500
Connection ~ 4150 3500
$EndSCHEMATC
